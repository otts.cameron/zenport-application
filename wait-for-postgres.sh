#!/bin/sh
# wait-for-postgres.sh

set -e

host="$1"
shift
user="$1"
shift
dbname="$1"
shift
cmd="$@"

#we sleep for 5 seconds here to give the server some time to start
echo sleeping 5
echo $host
echo $user
echo $cmd
sleep 3
until PGPASSWORD=$POSTGRES_PASSWORD psql "$dbname" -h "$host" -U "$user" -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "Postgres is up - executing command"
exec $cmd
