package testing

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"

	"github.com/ory/dockertest"
)

type TestDatabase struct {
	pool     *dockertest.Pool
	resource *dockertest.Resource
}

func CreateTestDatabase(user, password, dbName string) *TestDatabase {
	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatal("Could not connect to docker: ", err)
	}
	resource, err := pool.Run("postgres", "9.6", []string{fmt.Sprintf("POSTGRES_PASSWORD=%s", password), fmt.Sprintf("POSTGRES_USER=%s", user), fmt.Sprintf("POSTGRES_DB=%s", dbName)})
	if err != nil {
		log.Fatal("Could not start resource: ", err)
	}

	var db *sql.DB
	if err = pool.Retry(func() error {
		var err error
		db, err = sql.Open("postgres", fmt.Sprintf("postgres://%s:%s@localhost:%s/%s?sslmode=disable", user, password, resource.GetPort("5432/tcp"), dbName))
		if err != nil {
			return err
		}
		return db.Ping()
	}); err != nil {
		log.Fatalf("Could not connect to docker: %s", err)
	}

	f, err := ioutil.ReadFile("../../database/migrations/init.sql")
	if err != nil {
		log.Fatal("Unable to read db migration: ", err)
	}
	if _, err := db.Exec(string(f)); err != nil {
		log.Fatal("Unable to execute db migration: ", err)
	}
	return &TestDatabase{pool: pool, resource: resource}
}

func (td *TestDatabase) Purge() error {
	return td.pool.Purge(td.resource)
}

func (td *TestDatabase) GetPort() string {
	return td.resource.GetPort("5432/tcp")
}
