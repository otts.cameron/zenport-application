package server

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"reflect"
	"strconv"
)

func ErrorResponse(request *http.Request, response http.ResponseWriter, status int, message string) {
	//Need to drain request body if we return before reading
	if request.Body != nil {
		if cl, e := strconv.ParseInt(request.Header.Get("Content-Length"), 10, 64); e == nil && cl > 0 {
			io.Copy(ioutil.Discard, request.Body)
		}
	}

	if request.Method == http.MethodHead {
		//if head request must send message in header
		response.Header().Set("Error", message)
		response.WriteHeader(status)
	} else {
		content := []byte(message)
		response.Header().Set("Content-Length", strconv.Itoa(len(content)))
		response.Header().Set("Content-Type", "text/plain")
		response.WriteHeader(status)
		response.Write(content)
	}
}

func JsonResponse(request *http.Request, response http.ResponseWriter, data interface{}, status int) {
	content, err := json.Marshal(data)
	if err != nil {
		ErrorResponse(request, response, http.StatusInternalServerError, fmt.Sprintf("Error generating JSON response. (error: %s)", err))
		return
	}
	if string(content) == "null" {
		content = convertJsonNull(content, data)
	}
	response.Header().Set("Content-Type", "application/json")
	response.Header().Set("Content-Length", strconv.Itoa(len(content)))
	response.WriteHeader(status)
	response.Write(content)
}

func convertJsonNull(content []byte, data interface{}) []byte {
	rt, rv := reflect.TypeOf(data), reflect.ValueOf(data)
	switch rt.Kind() {
	case reflect.Chan, reflect.Func, reflect.Interface, reflect.Map, reflect.Ptr:
		if rv.IsNil() {
			return []byte("")
		}
	case reflect.Array, reflect.Slice:
		if rv.IsNil() {
			return []byte("[]")
		}
	}
	return content
}
