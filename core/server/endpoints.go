package server

import (
	"net"
)

type Endpoint struct {
	Listener *StoppableNetListener
}

type Configuration struct {
	Address string
	Path    string
}

func CreateEndpoints(cfg ...Configuration) ([]Endpoint, error) {
	var ret []Endpoint
	for _, ep := range cfg {
		listener, err := CreateTcpEndpoint(ep.Address)
		if err != nil {
			return nil, err
		}
		ret = append(ret, Endpoint{listener})
	}
	return ret, nil
}

func CreateTcpEndpoint(address string) (*StoppableNetListener, error) {
	l, err := net.Listen("tcp", address)
	if err != nil {
		return nil, err
	}
	return NewStoppableListener(l.(*net.TCPListener)), nil
}
