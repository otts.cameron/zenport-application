package server

import (
	"fmt"
	"net"
	"time"
)

var ErrServerStopped = fmt.Errorf("Network listener stopped, server exiting")

type DeadlinedListener interface {
	net.Listener
	SetDeadline(time.Time) error
}

type StoppableNetListener struct {
	listener DeadlinedListener
	wait     int64
	stop     chan struct{}
}

func NewStoppableListener(l DeadlinedListener) *StoppableNetListener {
	return &StoppableNetListener{
		listener: l,
		wait:     3,
		stop:     make(chan struct{}),
	}
}

func (sl *StoppableNetListener) Accept() (net.Conn, error) {
	for {
		d := int64(time.Second) * sl.wait
		sl.listener.SetDeadline(time.Now().Add(time.Duration(d)))

		nc, err := sl.listener.Accept()
		select {
		case <-sl.stop:
			return nil, ErrServerStopped
		default:
		}

		if err != nil {
			if e, ok := err.(net.Error); ok && e.Timeout() && e.Temporary() {
				continue
			}
		}
		return nc, err
	}
}

func (sl *StoppableNetListener) Stop() {
	close(sl.stop)
}

func (sl *StoppableNetListener) Close() error {
	return sl.listener.Close()
}

func (sl *StoppableNetListener) Addr() net.Addr {
	return sl.listener.Addr()
}
