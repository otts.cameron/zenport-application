CREATE SCHEMA IF NOT EXISTS colosseum;

CREATE TABLE IF NOT EXISTS colosseum.knight (
    knight_id serial NOT NULL,
    name text NOT NULL,
    power numeric NOT NULL,
    weapon numeric NOT NULL,
    CONSTRAINT pk_knight
        PRIMARY KEY (knight_id)
)
