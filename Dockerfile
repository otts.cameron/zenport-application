# My dockerfile
FROM golang:onbuild

RUN apt-get update && apt-get install -y postgresql postgresql-contrib
COPY wait-for-postgres.sh wait-for-postgres.sh