package handlers

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"

	"gitlab.com/otts.cameron/zenport-application/core/server"
	"gitlab.com/otts.cameron/zenport-application/domain"
	"gitlab.com/otts.cameron/zenport-application/engine"

	"github.com/gorilla/mux"
)

func HandleKnightRequest(eng engine.Engine) http.HandlerFunc {
	return func(response http.ResponseWriter, request *http.Request) {
		switch request.Method {
		case http.MethodGet:
			getKnightList(response, request, eng)
		case http.MethodPost:
			saveKnight(response, request, eng)
		default:
			server.ErrorResponse(request, response, http.StatusBadRequest, fmt.Sprintf("Invalid HTTP Method sent: %s", request.Method))
		}
	}
}

func HandleKnightIDRequest(eng engine.Engine) http.HandlerFunc {
	return func(response http.ResponseWriter, request *http.Request) {
		switch request.Method {
		case http.MethodGet:
			getKnightByID(response, request, eng, mux.Vars(request))
		default:
			server.ErrorResponse(request, response, http.StatusBadRequest, fmt.Sprintf("Invalid HTTP Method sent: %s", request.Method))
		}
	}
}

func getKnightList(response http.ResponseWriter, request *http.Request, eng engine.Engine) {
	knights, err := eng.ListKnights()
	if err != nil {
		server.ErrorResponse(request, response, http.StatusNotFound, fmt.Sprintf("Error getting knights list: %s", err))
	}
	server.JsonResponse(request, response, knights, http.StatusOK)
}

func saveKnight(response http.ResponseWriter, request *http.Request, eng engine.Engine) {
	knight := domain.Knight{}
	dataLength, err := strconv.Atoi(request.Header.Get("Content-Length"))
	if err != nil || dataLength <= 0 {
		server.ErrorResponse(request, response, http.StatusLengthRequired, "The 'Content-Length' HTTP header is required")
		return
	}
	data, err := ioutil.ReadAll(io.LimitReader(request.Body, int64(dataLength)))
	err = json.Unmarshal(data, &knight)
	if err != nil {
		server.ErrorResponse(request, response, http.StatusBadRequest, err.Error())
		return
	}
	err = eng.SaveKnight(&knight)
	if err != nil {
		server.ErrorResponse(request, response, http.StatusBadRequest, fmt.Sprintf("Error saving knight: %s", err))
	}
	server.JsonResponse(request, response, knight, http.StatusCreated)
}

func getKnightByID(response http.ResponseWriter, request *http.Request, eng engine.Engine, vars map[string]string) {
	var knightID int
	var err error
	if id, ok := vars["id"]; ok {
		knightID, err = strconv.Atoi(id)
		if err != nil {
			server.ErrorResponse(request, response, http.StatusBadRequest, fmt.Sprintf("Error parsing ID. Expected integer: %s", err))
		}
	}
	knight, err := eng.GetKnight(knightID)
	fmt.Println("Tried getting knight")
	if err != nil {
		server.ErrorResponse(request, response, http.StatusNotFound, fmt.Sprintf("Knight #%s not found.", vars["id"]))
	}
	server.JsonResponse(request, response, knight, http.StatusOK)
}
