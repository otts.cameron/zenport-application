# Notes
The Postgres Database and Application can be run through the included docker-compose file. The app defaultly listens on :8080

*Database setup, structure change, tests setup, etc.*
core/server/endopints was written with the intent of having multiple types of listeners. For example listening on multiple ports or allowing for unix sockets.

Refactored most DB interactions to propagate an error for potential error handling

Despite the instruction doc saying the fighter info should have been ints I intentionally left them as floats. I felt like this left us open for potential expansion later.
Similar to above I change the GetID interface to return an int. This was made to increase database performace. Considering there was no explicit requirement for using GUIDS a Serial Database ID would grant us the best performance.

Most of the test changes were to facilitate things such as error propogation and dependency injection.