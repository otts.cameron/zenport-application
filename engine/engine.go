package engine

import "gitlab.com/otts.cameron/zenport-application/domain"

type Engine interface {
	GetKnight(ID int) (*domain.Knight, error)
	ListKnights() ([]*domain.Knight, error)
	SaveKnight(knight *domain.Knight) error
	Fight(fighter1ID int, fighter2ID int) (domain.Fighter, error)
}

type KnightRepository interface {
	Find(ID int) (*domain.Knight, error)
	FindAll() ([]*domain.Knight, error)
	Save(knight *domain.Knight) error
}

type DatabaseProvider interface {
	GetKnightRepository() KnightRepository
}

type arenaEngine struct {
	arena            *domain.Arena
	knightRepository KnightRepository
}

func NewEngine(db DatabaseProvider) Engine {
	return &arenaEngine{
		arena:            &domain.Arena{},
		knightRepository: db.GetKnightRepository(),
	}
}
