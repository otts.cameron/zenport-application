package engine

import (
	"fmt"

	"gitlab.com/otts.cameron/zenport-application/domain"
)

func (engine *arenaEngine) GetKnight(ID int) (*domain.Knight, error) {
	fighter, err := engine.knightRepository.Find(ID)
	if err != nil {
		return nil, err
	}
	if fighter == nil {
		return nil, fmt.Errorf("fighter with ID '%d' not found!", ID)
	}

	return fighter, nil
}

func (engine *arenaEngine) ListKnights() ([]*domain.Knight, error) {
	return engine.knightRepository.FindAll()
}

func (engine *arenaEngine) SaveKnight(knight *domain.Knight) error {
	return engine.knightRepository.Save(knight)
}

func (engine *arenaEngine) Fight(fighter1ID int, fighter2ID int) (domain.Fighter, error) {
	f1, err := engine.GetKnight(fighter1ID)
	if err != nil {
		return nil, err
	}
	f2, err := engine.GetKnight(fighter2ID)
	if err != nil {
		return nil, err
	}
	return engine.arena.Fight(f1, f2), nil
}
