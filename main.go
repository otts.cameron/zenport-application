package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/otts.cameron/zenport-application/adapters/http"
	"gitlab.com/otts.cameron/zenport-application/engine"
	"gitlab.com/otts.cameron/zenport-application/providers/database"
)

func main() {
	var host = flag.String("host", "", "Host address to listen on")
	var port = flag.String("port", "80", "Host port to listen on")
	flag.Parse()
	provider, err := database.NewProvider("db", "5432", "colosseum", "zenport-application", "zenport-application")
	if err != nil {
		log.Fatal(err)
	}
	e := engine.NewEngine(provider)

	fmt.Println("Starting server on", *host, ":", *port)
	adapter, err := http.NewHTTPAdapter(e, *host, *port)
	if err != nil {
		log.Fatal(err)
	}

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, syscall.SIGINT, syscall.SIGTERM)
	defer close(stop)

	adapter.Start()
	//err = provider.GetKnightRepository().Save(&domain.Knight{Strength: 12.5, WeaponPower: 13.64})
	if err != nil {
		log.Fatal(err)
	}

	<-stop

	adapter.Stop()
	adapter.Wait()
	adapter.Close()
	provider.Close()
}
