package database

import (
	"fmt"

	"gitlab.com/otts.cameron/zenport-application/domain"

	"github.com/jmoiron/sqlx"
)

type knightRepository struct {
	connection *sqlx.DB
}

func (repository *knightRepository) Find(ID int) (*domain.Knight, error) {
	var ret domain.Knight
	if repository.connection != nil {
		row := repository.connection.QueryRow("select knight_id, name, power, weapon from colosseum.knight where knight_id = $1", ID)
		if row != nil {
			err := row.Scan(&ret.ID, &ret.Name, &ret.Strength, &ret.WeaponPower)
			if err != nil {
				return nil, err
			}
		}
	}
	return &ret, nil
}

func (repository *knightRepository) FindAll() ([]*domain.Knight, error) {
	var ret []*domain.Knight
	if repository.connection != nil {
		rows, err := repository.connection.Query("select knight_id, name, power, weapon from colosseum.knight")
		if err != nil {
			return nil, err
		}
		for rows.Next() {
			var knight domain.Knight
			err := rows.Scan(&knight.ID, &knight.Name, &knight.Strength, &knight.WeaponPower)
			if err != nil {
				return nil, err
			}
			ret = append(ret, &knight)
		}
	}

	return ret, nil
}

func (repository *knightRepository) Save(knight *domain.Knight) error {
	if knight == nil {
		return fmt.Errorf("Cannot Save nil Knight")
	}
	if repository.connection != nil {
		_, err := repository.connection.Exec("insert into colosseum.knight (name, power, weapon) values ($1, $2, $3)", knight.Name, knight.Strength, knight.WeaponPower)
		return err
	}
	return fmt.Errorf("No connection to database")
}
