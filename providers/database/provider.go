package database

import (
	"fmt"

	"gitlab.com/otts.cameron/zenport-application/engine"

	//Postgres SQL Driver
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type Provider struct {
	connection *sqlx.DB
}

func (provider *Provider) GetKnightRepository() engine.KnightRepository {
	return &knightRepository{connection: provider.connection}
}

func (provider *Provider) Close() {
	provider.connection.Close()
}

func NewProvider(host, port, dbname, user, password string) (*Provider, error) {
	db, err := sqlx.Open("postgres", fmt.Sprintf("host='%s' dbname='%s' user='%s' password='%s' port='%s' sslmode='disable'", host, dbname, user, password, port))
	if err != nil {
		return nil, err
	}
	return &Provider{connection: db}, db.Ping()
}
