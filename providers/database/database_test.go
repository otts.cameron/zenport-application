package database

import (
	"log"
	"os"
	"testing"

	coretesting "gitlab.com/otts.cameron/zenport-application/core/testing"
	"gitlab.com/otts.cameron/zenport-application/domain"
)

var prov *Provider

func TestMain(m *testing.M) {
	tdb := coretesting.CreateTestDatabase("int-test", "int-test", "colosseum")
	var err error
	prov, err = NewProvider("localhost", tdb.GetPort(), "colosseum", "int-test", "int-test")
	if err != nil {
		log.Fatal("Could not connect to database: ", err)
	}
	code := m.Run()

	if err := tdb.Purge(); err != nil {
		log.Fatalf("Could not purge resource: %s", err)
	}
	os.Exit(code)
}
func TestGetKnihtsReturnsAnEmptyListWithNoResults(t *testing.T) {
	knights, err := prov.GetKnightRepository().FindAll()
	if err != nil {
		t.Fatalf("Unexpected error: %s", err)
	}
	if len(knights) > 0 {
		t.Fatalf("Expected 0 knights, got %d", len(knights))
	}
}

func TestSaveKnightReturnsErrorOnNil(t *testing.T) {
	err := prov.GetKnightRepository().Save(nil)
	if err == nil {
		t.Fatal("Expected Repository to return error with nil knight")
	}
}

func TestSaveKnightReturnsNoErrorOnSucess(t *testing.T) {
	err := prov.GetKnightRepository().Save(&domain.Knight{Name: "Sir Knight", Strength: 12, WeaponPower: 20})
	if err != nil {
		t.Fatal("Expected Repository to not return error with populated knight")
	}
}

func TestGetKnightByIDReturnsNilOnNotFound(t *testing.T) {
	knight, err := prov.GetKnightRepository().Find(20)
	if err == nil {
		t.Fatal("Expected not found errors")
	}
	if knight != nil {
		t.Fatal("Expected knight to be nil")
	}
}

func TestGetKnightByIDReturnsExpectedKnight(t *testing.T) {
	knight, err := prov.GetKnightRepository().Find(1)
	if err != nil {
		t.Fatalf("Expected no errors, got %s", err)
	}
	if knight.Name != "Sir Knight" {
		t.Fatalf("Got name mismatch. Expected 'Sir Knight', got %q", knight.Name)
	}
	if knight.Strength != 12 {
		t.Fatalf("Got strength mismatch. Expected 12, got %f", knight.Strength)
	}
	if knight.WeaponPower != 20 {
		t.Fatalf("Got WeaponPower mismatch. Expected 20, got %f", knight.WeaponPower)
	}
}
