package domain

type Fighter interface {
	GetID() int
	GetPower() float64
}

type Knight struct {
	ID          int
	Name        string
	Strength    float64
	WeaponPower float64
}

func (k *Knight) GetID() int {
	return k.ID
}

func (k *Knight) GetPower() float64 {
	return k.Strength + k.WeaponPower
}
