# Questions

For this assignment you also have to answer a couple of questions.
There is no correct answer and none is mandatory, if you don't know just skip it.

 - **What do you think of the initial project structure ?**
I think the overall structure is good. The names weren't really meaningful, but that isn't a huge issue.


 - **What you will improve from your solution ?**
I would handle configuration better. Probably use something like Viper.
Additionally I would like some cobra integration to have a server and cli tools with the same executable.
I really don't like the default go logger, so I'd probably replace it with something else.

 - **For you, what are the boundaries of a service inside a micro-service architecture ?**
Do only what you need to do. If you find yourself doing too much it might be time to think about breaking it off.


 - **For you, what are the most relevant usage for SQL, NoSQL, key-value and document store ?**


