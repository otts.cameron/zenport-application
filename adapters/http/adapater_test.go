package http

import (
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strconv"
	"testing"

	"bytes"

	"encoding/json"

	coretesting "gitlab.com/otts.cameron/zenport-application/core/testing"
	"gitlab.com/otts.cameron/zenport-application/domain"
	"gitlab.com/otts.cameron/zenport-application/engine"
	"gitlab.com/otts.cameron/zenport-application/providers/database"
)

var (
	router http.Handler
)

func TestMain(m *testing.M) {
	tdb := coretesting.CreateTestDatabase("int-test", "int-test", "colosseum")
	provider, err := database.NewProvider("localhost", tdb.GetPort(), "colosseum", "int-test", "int-test")
	if err != nil {
		log.Fatal("Could not connect to database: ", err)
	}
	e := engine.NewEngine(provider)

	router = ConfigureRouter(e)

	code := m.Run()
	provider.Close()
	if err := tdb.Purge(); err != nil {
		log.Fatalf("Could not purge resource: %s", err)
	}
	os.Exit(code)
}

func TestPostKnightBipolelm(t *testing.T) {
	body := []byte(`{"name":"Bipolelm","strength":10,"weapon_power":20}`)
	req, err := http.NewRequest(http.MethodPost, "/knight", bytes.NewBuffer(body))
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Content-Length", strconv.Itoa(len(body)))
	if err != nil {
		t.Fatal(err)
	}

	recorder := httptest.NewRecorder()
	router.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusCreated {
		t.Fatal("Server error: Returned ", recorder.Code, " instead of ", http.StatusCreated)
	}
}

func TestPostKnightElrynd(t *testing.T) {
	body := []byte(`{"name":"Elrynd","strength":10,"weapon_power":50}`)
	req, err := http.NewRequest(http.MethodPost, "/knight", bytes.NewBuffer(body))
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Content-Length", strconv.Itoa(len(body)))
	if err != nil {
		t.Fatal(err)
	}

	recorder := httptest.NewRecorder()
	router.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusCreated {
		t.Fatal("Server error: Returned ", recorder.Code, " instead of ", http.StatusCreated)
	}
}

func TestPostKnightDefaults(t *testing.T) {
	body := []byte(`{"name":"Knighty"}`)
	req, err := http.NewRequest(http.MethodPost, "/knight", bytes.NewBuffer(body))
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Content-Length", strconv.Itoa(len(body)))
	if err != nil {
		t.Fatal(err)
	}

	recorder := httptest.NewRecorder()
	router.ServeHTTP(recorder, req)

	knight := domain.Knight{}

	if err := json.Unmarshal(recorder.Body.Bytes(), &knight); err != nil {
		t.Fatal(err)
	}

	if knight.Name != "Knighty" {
		t.Fatal("Knight did not properly save")
	}
}

func TestPostKnightBadType(t *testing.T) {
	body := []byte(`name:"Bipolelm"`)
	req, err := http.NewRequest(http.MethodPost, "/knight", bytes.NewBuffer(body))
	req.Header.Add("Content-Type", "text/plain")
	req.Header.Add("Content-Length", strconv.Itoa(len(body)))
	if err != nil {
		t.Fatal(err)
	}

	recorder := httptest.NewRecorder()
	router.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusBadRequest {
		t.Fatal("Server error: Returned ", recorder.Code, " instead of ", http.StatusBadRequest)
	}
}

func TestGetKnights(t *testing.T) {
	req, err := http.NewRequest(http.MethodGet, "/knight", nil)
	if err != nil {
		t.Fatal(err)
	}

	recorder := httptest.NewRecorder()
	router.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusOK {
		t.Fatal("Server error: Returned ", recorder.Code, " instead of ", http.StatusOK)
	}

	var response []map[string]interface{}

	if err := json.NewDecoder(recorder.Body).Decode(&response); err != nil {
		t.Fatal(err)
	}

	if len(response) != 3 {
		t.Fatal("Response error: Expected 3 knights")
	}

	knight := response[0]
	fmt.Println(knight)

	if _, has := knight["ID"]; !has {
		t.Fatal("Response error: Expected ID field in knight object")
	}
	if _, has := knight["Name"]; !has {
		t.Fatal("Response error: Expected Name field in knight object")
	}
	if _, has := knight["Strength"]; !has {
		t.Fatal("Response error: Expected Strength field in knight object")
	}
	if _, has := knight["WeaponPower"]; !has {
		t.Fatal("Response error: Expected WeaponPower field in knight object")
	}

	if response[0]["ID"].(float64) == response[1]["ID"].(float64) {
		t.Fatal("Response error: Expected not same ID for each knights")
	}
}

func TestGetKnightNotFound(t *testing.T) {
	req, err := http.NewRequest(http.MethodGet, "/knight/123456789", nil)
	if err != nil {
		t.Fatal(err)
	}

	recorder := httptest.NewRecorder()
	router.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusNotFound {
		t.Fatal("Server error: Returned ", recorder.Code, " instead of ", http.StatusNotFound)
	}

	if recorder.Body.String() != "Knight #123456789 not found." {
		t.Fatalf("Response error: Expected error message 'Knight #123456789 not found.', got %q", recorder.Body.String())
	}
}
