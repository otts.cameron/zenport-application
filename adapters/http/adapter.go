package http

import (
	"fmt"
	"net/http"
	"sync"

	"gitlab.com/otts.cameron/zenport-application/core/server"
	"gitlab.com/otts.cameron/zenport-application/engine"
	"gitlab.com/otts.cameron/zenport-application/handlers"

	"github.com/gorilla/mux"
)

type HTTPAdapter struct {
	endpoints []server.Endpoint
	engine    engine.Engine
	wg        sync.WaitGroup
}

func (adapter *HTTPAdapter) Start() {
	fmt.Println("Starting server!")

	adapter.wg.Add(len(adapter.endpoints))
	for _, ep := range adapter.endpoints {
		go func(endpoint server.Endpoint) {
			defer adapter.wg.Done()
			router := ConfigureRouter(adapter.engine)
			srv := http.Server{Handler: router}
			if err := srv.Serve(endpoint.Listener); err != nil && err != server.ErrServerStopped {
				fmt.Println("Error from endpoint:", err)
			}
		}(ep)
	}
}

func (adapter *HTTPAdapter) Stop() {
	fmt.Println("Stopping server!")
	for _, e := range adapter.endpoints {
		e.Listener.Stop()
	}

}

func (adapter *HTTPAdapter) Close() {
	fmt.Println("Closing Server")
	for _, e := range adapter.endpoints {
		e.Listener.Close()
	}
}

func (adapter *HTTPAdapter) Wait() {
	adapter.wg.Wait()
}

func NewHTTPAdapter(e engine.Engine, host, port string) (*HTTPAdapter, error) {
	// todo: init your http server and routes
	ep, err := server.CreateEndpoints(server.Configuration{Address: fmt.Sprintf("%s:%s", host, port)})
	return &HTTPAdapter{
		endpoints: ep,
		engine:    e,
	}, err
}

func ConfigureRouter(e engine.Engine) *mux.Router {
	router := mux.NewRouter()

	CreateSubroutes(router, e)
	router.NotFoundHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		server.ErrorResponse(r, w, http.StatusNotFound, fmt.Sprintf("Invalid request URL: %s", r.URL.String()))
	})

	return router
}

func CreateSubroutes(router *mux.Router, e engine.Engine) {
	router.HandleFunc("/knight", handlers.HandleKnightRequest(e)).Methods(http.MethodGet, http.MethodPost)
	router.HandleFunc("/knight/{id:[0-9]+}", handlers.HandleKnightIDRequest(e)).Methods(http.MethodGet)
}
